
library(shiny)
library(glmnet)
library(dplyr)

load(file="./day30mort.rda")
cardiacCof <- coef(cardiac.shiny)[, 1]
names <- names(tail(cardiacCof, -2))


shinyServer(function(input, output) {
    inputdata <- reactive({
        fieldValues <- c(0)
        for (name in names) {
            fieldValues <- c(fieldValues, input[[name]])
        }
        fieldValues;
    })
    output$result <- renderUI({
        result <- "Results: <br />";
        inputs <- inputdata();
        m <- matrix(inputs, 1);
        for (lambda in day30mort$lambda) {
            prediction <- predict.glmnet(day30mort, newx=m, s=lambda)
            result <- sprintf("%sModel Lambda: %f &nbsp&nbsp&nbsp result: %f<br />", result, lambda, prediction)
        }
        HTML(result)
    })
})
